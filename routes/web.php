<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FirstController@login');

Route::get('/welcome', function(){
    return 'asdasdsd';
});
Route::get('checkDB', function ()
{
    dd(DB::connection()->getDatabaseName());
});
Route::get('thongtin/{tuoi}/{ten}', function($tuoi, $ten) {
   return "hello $ten , $tuoi tuổi";
})->where(['tuoi' => '[0-9]+', 'ten' => '[a-z]+']);
Route::get('test',function(){
	return view('test');
});
Route::resource('task','crud');
Route::get('add', 'FirstController@add');
Route::post('store','FirstController@store');
Route::get('login', 'FirstController@login');
Route::post('check','loginController@check');
Route::get('logout','loginController@logout');
Route::get('category/{loai}','category@index');
Route::get('angular', function(){
    return view('angular');
});
Route::post('timkiem','FirstController@search');
Route::get('json','FirstController@angular');
Route::post('post','FirstController@guiapi');
