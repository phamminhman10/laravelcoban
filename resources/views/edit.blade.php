@extends('layout')
@section('content')
<div class="container">
		<div class="row">
			@foreach($data as $p)
			
				<h2>EDIT</h2>
				<form action="{{route('task.update',$p->so)}}" method="post" enctype="multipart/form-data">		
			{{csrf_field()}}
			{{ method_field('PUT') }}	
				<div class="col-sm-5">
					<div class="form-group">
						<label for="">So: </label>
						<input type="text" class="form-control" name="so" value="{{$p->so}}">
					
					</div>
					<div class="form-group">
						<label for="">Gia</label>
						<input type="text" class="form-control" name="gia" value="{{$p->gia}}" >
						 @if($errors->has('gia'))
                			 <p style="color:red">{{$errors->first('gia')}}</p>
               			 @endif
					</div>
					<div class="form-group">
						<label for="">Loai</label>
						<select class="form-control" name="loai">
                    		<option value="Mobi">Mobi</option>
                   			<option value="Vina">Vina</option>
                 		    <option value="Viettl">Viettel</option>
                 	     </select>
					</div>
					

                	<div class="card">
                		<p for="">Upload anh <i class="fas fa-image"></i> </p>
                		<img src="../../{{$p->anh}}" width="150px" height="100px">      					
              			<input name="anh" class="form-control" type="file" required="true">
                	</div>
                	<br>
					<button type="submit" class="btn btn-success">Update</button>
				</div>
				
			</form>
			
			@endforeach
		</div>
	</div>
	@endsection