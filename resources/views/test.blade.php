@extends('layout')
@section('content')

@if(Session::has('name'))
<div class="container">
  <nav class="navbar navbar-expand-sm navbar-dark bg-primary">
    <a class="navbar-brand" href="#">Tai khoan la: {{ Session::get('name') }}</a>
    <a class="navbar-brand"  href="{{url('logout')}}">Logout <i class="fas fa-sign-out-alt"></i></a>
  </nav>  

</div>


<div class="container">
  <div class="text-xl-center">
    <h3 class="display-4">Danh sach</h3>
    <h4>Tim kiem</h4>
    <form action="{{url('timkiem')}}" method="POST">
     {{ csrf_field()}}
     <input type="text" class="form-control" name="searchText" >
     <br>
     <button type="submit" class="btn btn-primary">Search</button>
   </form>
   <hr>
 </div>
</div>
<div class="container">
  <div class="row">
   @foreach ($db as $p)   
   <div class="col-sm-2"> 
    <div class="card">
      <img class="card-img-top" src="{{$p->anh}}" width="150px" height="100px" >       
      <h5 class="card-title ten">so:{{$p->so}}</h5>
      <p class="card-text tuoi">gia:{{$p->gia}}</p>
      <a href="{{route('task.edit',$p->so)}}" class="btn btn-warning"><i class="far fa-edit"></i></a>
      <a href="" class="btn btn-danger"><i class="fa fa-times"></i></a>

      <p class="card-text"><small class="text-muted">Loai:{{$p->loai}}</small></p>
    </div>  
  </div> 
  @endforeach
</div>  
</div>

{{ $db->links() }}
<h2>PHP LARA</h2>
<a href="{{route('task.create')}}" class="btn btn-primary">Add</a>
@foreach ($loaisp as $q)
<a href="{{url('category',$q->tenloai)}}"    class="btn btn-success">{{$q->tenloai}}</a>
@endforeach

<table class="table table-striped">
  <thead>
   <tr>
    <th>So </th>
    <th>Gia</th>
    <th>Loai</th>
    <th>IMG</th>
    <th>Action</th>
  </tr>
</thead>
<tbody>

 @foreach ($db as $p)
 <tr >
   <td >{{$p->so}}</td>
   <td>{{$p->gia}}</td>
   <td>{{$p->loai}}</td>
   <td><img src="{{$p->anh}}" width="150px" height="100px"></td>
   <td>
    <form action="{{route('task.destroy',$p->so)}}" method="post">
      @csrf
      @method('DELETE')
      <a href="{{route('task.edit',$p->so)}}" class="btn btn-warning"><i class="far fa-edit"></i></a>
      <button type="submit" class="btn btn-success"><i class="fa fa-times"></i></button>
    </form>
  </td>
</tr>

</tbody>
@endforeach
</table>
@endif




@endsection