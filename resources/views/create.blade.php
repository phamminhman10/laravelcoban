@extends('layout')

@section('content')

<form action="{{route('task.store')}}" method="POST" enctype="multipart/form-data">
     {{ csrf_field()}}
  
     <div class="row">
        <div >
            <div class="form-group">
                <strong>so:</strong>
                <input type="hidden" name="so" class="form-control" placeholder="so">
            </div>
        </div>
        <div >
            <div class="form-group">
                <strong>gia:</strong>
                <input type="text" class="form-control"  name="gia" placeholder="gia" ></div>
                @if($errors->has('gia'))
                 <p style="color:red">{{$errors->first('gia')}}</p>
                @endif
        </div>
        <div >
            <div class="form-group">
                <strong>Loai:</strong>
                <select class="form-control" name="loai">
                    <option value="Mobi">Mobi</option>
                    <option value="Vina">Vina</option>
                    <option value="Viettel">Viettel</option>
                </select>
        </div>
         <div>
              		<label for="">Upload anh <i class="fa fa-photo"></i> </label>
              		<input name="anh" class="form-control" type="file" required="true">
         </div>
        </div>
       <br>
        <div >
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
@endsection