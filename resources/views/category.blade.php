@extends('layout')
@section('content')
<div class="container">
	<div class="text-xs-center">
		<h3 class="display-4">LOAI SAN PHAM</h3>
		<hr>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-9 push-sm-1">
			<table class="table table-striped">
				<thead>
					<tr>
						
						<th scope="col">Mã :</th>
						<th scope="col">Gia :</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($get_loai as $p)
					<tr>
						<td>{{$p->so}}</td>
						<td>{{$p->gia}}</td>

					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection