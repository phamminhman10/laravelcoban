<!DOCTYPE html>
<html lang="en"  >
<head>
  <title>test angular</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="vendor/angular-material.min.css">
</head>
<body ng-app="myApp" ng-controller="MyController">

  <div class="khoi1 cards" ng-show="hienthi">
    <div class="container">   
       <button class="btn btn-info" ng-click="doigiatri()">Add</button>
    </div>
  </div>
  
  <div class="khoi2 cards" ng-show="!hienthi">
    <div class="container">
      <div class="row">
        <div class="col-xl-4">
             <div class="card border-primary">
                    <div class="card-header">Add </div>
                    <div class="card-body text-primary">
                        <div class="form-group">
                             <label for="">Gia</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="">Loai nha mang</label>
                            <select class="form-control">
                                <option>Mobi</option>
                                <option>Vina</option>
                                <option>Viettel</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Add</button>
                            <button class="btn btn-danger" ng-click="doigiatri()" >close</button>
                        </div>
                    </div>
                </div>
        </div>
      </div>
    </div>
    
  </div>
   <div class="container">
       <div class="row">
         <div class="col-xl-4">
             <h1>Live search</h1>
             <input type="text" ng-model="searchText" class="form-control">
           </div>
       </div>
   </div>



    <div class="container">
        <div class="row">
           
            <md-autocomplete
          ng-disabled="ctrl.isDisabled"
          md-no-cache="ctrl.noCache"
          md-selected-item="ctrl.selectedItem"
          md-search-text-change="ctrl.searchTextChange(ctrl.searchText)"
          md-search-text="ctrl.searchText"
          md-selected-item-change="ctrl.selectedItemChange(mot)"
          md-items="mot in nhieunguoi"
          md-item-text="mot.gia"
          md-min-length="0"
          placeholder="Nhap vao tu khoa tim kiem">
        <md-item-template>
          <span md-highlight-text="ctrl.searchText" md-highlight-flags="^i">@{{mot.gia}}</span>
        </md-item-template>
        <md-not-found>
         
          <a ng-click="ctrl.newState(ctrl.searchText)">Create a new one!</a>
        </md-not-found>
      </md-autocomplete>
        </div>
    </div>
  <div class="container">
    <div class="row">
      <table class="table table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                    <thead>
                      <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                        Name
                        : activate to sort column descending" style="width: 171px;">
                       SO
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="
                      Position
                      : activate to sort column ascending" style="width: 207px;">
                      Gia
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="
                    Office
                    : activate to sort column ascending" style="width: 174px;">
                    Loai nha mang
                  </th>
                  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="
                  Age
                  : activate to sort column ascending" style="width: 140px;">
                  Image
                </th>
                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="
                Start Date
                : activate to sort column ascending" style="width: 240px;">
                Action
              </th>
            </tr>
          </thead>
      <tbody ng-repeat="mot in nhieunguoi|filter:searchText""  ng-init="mot.hienthi=false">
        <tr ng-show="!mot.hienthi">
          <td>@{{mot.so}}</td>
          <td>@{{mot.gia}}</td>
          <td>@{{mot.loai}}</td>
          <td><img src="@{{mot.anh}}" width="100px" height="50px"></td>
          <td><button class="btn btn-warning" ng-click="hienra(mot)">edit</button></td>
        </tr>
        <!-- xem du lieu -->
        <tr ng-show="mot.hienthi">
          <td><input type="text" class="form-control" ng-model="mot.so" ></td>
          <td><input type="text" class="form-control" ng-model="mot.gia" name="gia"></td>
          <td><select ng-model="mot.loai" id="" class="form-control">
              <option value="Mobi">Mobi</option>
              <option value="Vina">Vina</option>
              <option value="Viettel">Viettel</option>
          </select></td>
          <td><input type="file" class="form-control" ng-model="mot.anh"></td>
          <td><button class="btn btn-primary" ng-click="luudulieu(mot)">save</button></td>
          <td><button class="btn btn-danger " ng-click="hienra(mot)">close</button></td>
        </tr>
      </tbody>

    </table>
  </div>
</div>


<script type="text/javascript" src="vendor/bootstrap.js"></script>  
<script type="text/javascript" src="vendor/angular-1.5.min.js"></script>  
<script type="text/javascript" src="vendor/angular-animate.min.js"></script>
<script type="text/javascript" src="vendor/angular-aria.min.js"></script>
<script type="text/javascript" src="vendor/angular-messages.min.js"></script>
<script type="text/javascript" src="vendor/angular-material.min.js"></script>  
<script type="text/javascript" src="vendor/1.js"></script>

</body>
</html>