<!DOCTYPE html>
<html>
<html lang="en" ng-app="my-app" >
<head>
	<title>Laravel</title>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
</head>
<body >
	
	<div class="container">
		@yield('content')
	</div>

	<script type="text/javascript" src="app/lib/angular.min.js"></script>  
	<script type="text/javascript" src="app/app.js"></script>  
</body>
</html>