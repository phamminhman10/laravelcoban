@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-6">Login</h1>
    <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
    <img src="https://via.placeholder.com/350x150" alt="">
  </div>
</div>
<div class="container">
		<div class="row">
			<div class="col-sm-6 ">
				<h4 class="alert alert-success"  style="text-align:center">LOGIN</h4>
			</div>
		</div>
</div>
	<div class="container">
  	<div class="row">
  		<div class="col-lg-6 push-sm-3">
  			<form action="{{url('check')}}" method="post">
              {{ csrf_field()}}
          	
  				<fieldset class="form-group">
              		<label for="">ID:</label>
              		<input name="id" type="text" class="form-control"  required="true">
  				</fieldset>
  				<fieldset class="form-group">
              		<label for="">PASS:</label>
              		<input name="pass" type="password" class="form-control"  required="true">
  				</fieldset>
  				<button type="submit" class="btn btn-danger">LOGIN</button>
  			</form>
  		</div>		
  	</div>
  </div>

@endsection