@extends('layout')
@section('content')
<div class="container">
  <div class="text-xl-center">
    <h3 class="display-4">KET QUA TRA VE</h3>

    <hr>
  </div>
</div>
<div class="container">
  <div class="row">
   @foreach ($kqtimkiem as $p)   
   <div class="col-sm-2"> 
    <div class="card">
      <img class="card-img-top" src="{{$p->anh}}" width="150px" height="100px" >       
      <h5 class="card-title ten">so:{{$p->so}}</h5>
      <p class="card-text tuoi">gia:{{$p->gia}}</p>
      <a href="{{route('task.edit',$p->so)}}" class="btn btn-warning"><i class="far fa-edit"></i></a>
      <a href="" class="btn btn-danger"><i class="fa fa-times"></i></a>

      <p class="card-text"><small class="text-muted">Loai:{{$p->loai}}</small></p>
    </div>  
  </div> 
  @endforeach
</div>  
</div>


@endsection