<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\sosim;
header('Access-Control-Allow-Origin: *');
class FirstController extends Controller
{
	public function add(){
		
		return view('create');
	}
	public function store(Request $req){
		$this->validate($req, [
			'so'=>'Required',
			'gia'=>'Required'
		]);
		$so=$req->input('so');
		$gia=$req->input('gia');
		
		echo $so;
		echo $gia;
		// DB::table('sosim')->insert(
		// 	['so' => $so, 'gia' => $gia]
		// );
	}
	public function login(){
		return view('login');
	}
	public function checklogin(Request $request){
		$id=$request->input('id');
		$pass=$request->input('pass');
		echo $id;
		echo $pass;
	}
	public function search(Request $request){
		$searchText=$request->input('searchText');	
     	$kqtimkiem=DB::table('sosim')->where('gia','like',"$searchText%")->get();
  		// echo $kqtimkiem;
        //=$->toArray();
    return view('search', compact('kqtimkiem'));
	}
	public function angular(){
		 $db=DB::table('sosim')->select('*')->get();
		  $db=json_encode($db);
		echo $db;
	}
	public function guiapi(Request $request){
		$so=$request->input('so');
		$gia=$request->input('gia');
		$loai=$request->input('loai');
		echo "post_angular";
		DB::table('sosim')->where('so',$so)->update(['gia'=>$gia,'loai'=>$loai]);
	}

}