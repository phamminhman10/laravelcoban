<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\sosim;
use App\loaisanpham;

class crud extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $data= "pkmm";
     $db=DB::table('sosim')->select('*')->get();
     $db=DB::table('sosim')->paginate(4);
     $loaisp=DB::table('loaisanpham')->select('*')->get();
    // echo $db;

        //=$->toArray();
     return view('test', compact('db','loaisp'));
      $db=json_encode($db);
       echo $db;
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $this->validate($req, ['gia'=>'Required|integer|min:2|max:10'], [
        'required' => ':attribute Không được để trống',
        'min' => ':attribute Không được nhỏ hơn :min',
        'max' => ':attribute Không được lớn hơn :max',
    ]);
     
        $so=$req->input('so');   
        $gia=$req->input('gia');      
        $loai=$req->input('loai');
     // $anh =$target_dir.$_FILES["anh"]["name"];
     //  move_uploaded_file($_FILES["anh"]["tmp_name"], $target_file);
    
     $file = $req->file('anh');
        echo $file;
     //Lấy Tên files
       echo 'Tên Files: '.$file->getClientOriginalName();
       echo '<br/>';

        //Lấy Đuôi File
       echo 'Đuôi file: '.$file->getClientOriginalExtension();
       echo '<br/>';

        //Lấy đường dẫn tạm thời của file
       echo 'Đường dẫn tạm: '.$file->getRealPath();
       echo '<br/>';

       //Lấy kích cỡ của file đơn vị tính theo bytes
       echo 'Kích cỡ file: '.$file->getSize();
       echo '<br/>';

       //Lấy kiểu file
       echo 'Kiểu files: '.$file->getMimeType();  
    $file->move('./upload/',$file->getClientOriginalName()); 
   // $duongdan = public_path('upload/');
    $anh="upload/".$file->getClientOriginalName();
      // echo $gia;
      // echo $anh;
      DB::table('sosim')->insert(['gia' => $gia,'anh'=>$anh,'loai'=>$loai]);
     return redirect('task');
       
        
    }       


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
      */
    public function edit($so)
    {
            $data=DB::table('sosim')->where('so',$so)->select('*')->get();
           // echo $data;
        return view('edit',compact('data'));
    }
    public function update(Request $request,$so){
        $this->validate($request, ['gia'=>'Required|integer|min:2|max:10'], [
        'required' => ':attribute Không được để trống',
        'min' => ':attribute Không được nhỏ hơn :min',
        'max' => ':attribute Không được lớn hơn :max',
    ]);
	//	$so=$request->input('so');
		$gia=$request->input('gia');
        $loai=$request->input('loai');
        $file = $request->file('anh');
        //dd($request->file('anh'));
      //  die();
        $file->move('./upload/',$file->getClientOriginalName()); 
   // $duongdan = public_path('upload/');
        $anh="upload/".$file->getClientOriginalName();
		DB::table('sosim')->where('so',$so)->update(['gia'=>$gia,'anh'=>$anh,'loai'=>$loai]);
	//	echo "$so";
      return redirect('task');
		
			
    }
    public function destroy($so)
    {
        // $blog = sosim::find($id);
        // $blog->delete();
        // return redirect('sosim');
        DB::table('sosim')->where('so',$so)->delete();
        return  redirect('task');
    }
    public function show(){
        //    return redirect('task');
	}
}
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    //}

